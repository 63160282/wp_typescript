type CarYear = number
type CarType = string
type CarModel = string
type Car = {
  year: CarYear,
  type: CarType,
  model: CarModel
}

const carYear: CarYear = 2001
const carType: CarType = "Toyota"
const carModel: CarModel = "Corolla"

const car1: Car = {
  year: 2002,
  type: "Nissan",
  model: "xxx"
}
const car2: Car = {
    year: 2004,
    type: "totota",
    model: "xxx"
  }
console.log(car1);
console.log(car2);
